pragma solidity ^0.4.7;

contract acme{
    ///States: 1-setPublicKeyBank--->2-setSignedAmount--->3-bankPayedUser--->4-govPayedBank
    ///key = h(NIF,importe, nºdefactura,R,fecha de emisión,período)
    ///value = [public_key_bank, encrypted_amount_ac_b, signed_data, state]
    mapping(uint256 => string[4]) cessions; 
    address acme_add;
    
 
    constructor() public {
        acme_add = msg.sender;
    }
	
    modifier isOwner () {
        require(msg.sender == acme_add);
        _;
    }
    
    //Des de front 
 
    //https://ethereum.stackexchange.com/questions/710/how-can-i-verify-a-cryptographic-signature-that-was-produced-by-an-ethereum-addr
    //https://medium.com/@angellopozo/ethereum-signing-and-validating-13a2d7cb0ee3
    //https://hackernoon.com/a-closer-look-at-ethereum-signatures-5784c14abecc
        

    //this function is for ACME setting the public key of the bank involved in the transaction
    function setPublicKeyBank(uint256 _hash, string pk_b)  public isOwner{
        cessions[_hash][0] = pk_b;
        cessions[_hash][3] = "1"; //change to state 1
    }

    //this function returns the public key in the mapping so they can check if it matches theirs
    function containsPublicKeyBank(uint256 _hash) public view returns (string){
        return cessions[_hash][0];
    }
    
    //FRONT: el bank veura la clau i comprovarà si es la seva
    
    //FRONT: check signatures (ha de comprovar que les firmes siguin correctes --> ens fiem 100% d'acme?)
    //Una vegada comprovades les firmes s'han d'encriptar les coses
    function setSignedAmount(uint256 _hash, string signedData, string encryptedAmount)  public isOwner{
        cessions[_hash][1] = encryptedAmount;
        cessions[_hash][2] = signedData;
        cessions[_hash][3] = "2"; //change to state 2
    }
    
    function viewEncryptedAmount(uint256 _hash) public view returns (string){
        return cessions[_hash][1];
    }
    
    function viewSignedData(uint256 _hash) public view returns (string){
        return cessions[_hash][2];
    }
    
    function bankPayedUser(uint256 _hash)  public isOwner{
        require(
            compareStrings(cessions[_hash][3], "2"),
            "You can't change to this state"
        );
        cessions[_hash][3] = "3"; //change to state 3
    }
    
    function govPayedBank(uint256 _hash)  public isOwner{
        require(
            compareStrings(cessions[_hash][3], "3"),
            "You can't change to this state"
        );
        cessions[_hash][3] = "4"; //change to state 4
    }
    
    function readState(uint256 _hash)  public view returns (string){
        return cessions[_hash][3];
    }
    
    function compareStrings (string a, string b) public view returns (bool){
       return keccak256(a) == keccak256(b);
   }
    
}
