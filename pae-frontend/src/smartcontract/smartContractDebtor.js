
import Web3 from 'web3';
const web3 = new Web3("http://localhost:8545");



// Smart contract abi
var abi_gene =[
	{
		"constant": false,
		"inputs": [
			{
				"name": "_hash",
				"type": "uint256"
			}
		],
		"name": "acceptInvoice",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"inputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "constructor"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "_hash",
				"type": "uint256"
			}
		],
		"name": "containsInvoice",
		"outputs": [
			{
				"name": "",
				"type": "bool"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	}
]
     
  
var contractAddress_Gene = "0x0d9c988a924a7bb1825b385fa83c4ab3f5110fed";
export const debtorSmartContract = new web3.eth.Contract(abi_gene, contractAddress_Gene);

//   //--------------------------BANK---------------------------------------------------
//      let  boolean= await  Sc.methods.containsInvoice( _hash).call(); // retorna true si encuentra el hash de la factura en la lista de facturas aceptadas de la gene

//   //--------------------------DEBTOR-------------------------------------------------
//      await  Sc.methods.acceptInvoice(_hash).send(); // no retorna nada
