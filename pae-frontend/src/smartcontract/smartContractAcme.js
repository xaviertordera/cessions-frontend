

import Web3 from 'web3';
const web3 = new Web3("http://localhost:8545");


var abi_Acme = [
	{
		"constant": false,
		"inputs": [
			{
				"name": "_hash",
				"type": "uint256"
			}
		],
		"name": "bankPayedUser",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "_hash",
				"type": "uint256"
			}
		],
		"name": "govPayedBank",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "_hash",
				"type": "uint256"
			},
			{
				"name": "pk_b",
				"type": "string"
			}
		],
		"name": "setPublicKeyBank",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "_hash",
				"type": "uint256"
			},
			{
				"name": "signedData",
				"type": "string"
			},
			{
				"name": "encryptedAmount",
				"type": "string"
			}
		],
		"name": "setSignedAmount",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"inputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "constructor"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "a",
				"type": "string"
			},
			{
				"name": "b",
				"type": "string"
			}
		],
		"name": "compareStrings",
		"outputs": [
			{
				"name": "",
				"type": "bool"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "_hash",
				"type": "uint256"
			}
		],
		"name": "containsPublicKeyBank",
		"outputs": [
			{
				"name": "",
				"type": "string"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "_hash",
				"type": "uint256"
			}
		],
		"name": "readState",
		"outputs": [
			{
				"name": "",
				"type": "string"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "_hash",
				"type": "uint256"
			}
		],
		"name": "viewEncryptedAmount",
		"outputs": [
			{
				"name": "",
				"type": "string"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "_hash",
				"type": "uint256"
			}
		],
		"name": "viewSignedData",
		"outputs": [
			{
				"name": "",
				"type": "string"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	}
]
 
const contractAddress_Acme = "0xf037761e6fce6e529a7d7dcc1f1ef3b6df20272a";
export const smartContracAcme = new web3.eth.Contract(abi_Acme,contractAddress_Acme);
 