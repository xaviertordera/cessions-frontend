pragma solidity ^0.4.7;

contract generalitat{
    mapping(uint256 => bool) accepted_hash; 
    address gene_add;

    constructor() public {
        gene_add = msg.sender;
    }
	
    modifier isOwner () {
        require(msg.sender == gene_add);
        _;
    }
	
	function acceptInvoice(uint256 _hash) public isOwner{
	    accepted_hash[_hash]=true;
    }

    function containsInvoice(uint256 _hash) public view returns (bool){
        return accepted_hash[_hash];
    }

}
