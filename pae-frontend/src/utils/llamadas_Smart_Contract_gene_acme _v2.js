
//imports necesarios para la comunicacion con la BC y creacion de la instancia del contrato
// instalar web3 en el directorio del proyecto : npm install web3
import Web3 from 'web3';
 const web3 = new Web3("http://localhost:8545");
 var abi_Acme = [
   {
     "inputs": [],
     "payable": false,
     "stateMutability": "nonpayable",
     "type": "constructor"
   },
   {
     "constant": false,
     "inputs": [
       {
         "name": "_hash",
         "type": "uint256"
       },
       {
         "name": "pk_b",
         "type": "string"
       }
     ],
     "name": "setPublicKeyBank",
     "outputs": [],
     "payable": false,
     "stateMutability": "nonpayable",
     "type": "function"
   },
   {
     "constant": true,
     "inputs": [
       {
         "name": "_hash",
         "type": "uint256"
       }
     ],
     "name": "containsPublicKeyBank",
     "outputs": [
       {
         "name": "",
         "type": "string"
       }
     ],
     "payable": false,
     "stateMutability": "view",
     "type": "function"
   },
   {
     "constant": false,
     "inputs": [
       {
         "name": "_hash",
         "type": "uint256"
       },
       {
         "name": "signedData",
         "type": "string"
       },
       {
         "name": "encryptedAmount",
         "type": "string"
       }
     ],
     "name": "setSignedAmount",
     "outputs": [],
     "payable": false,
     "stateMutability": "nonpayable",
     "type": "function"
   },
   {
     "constant": true,
     "inputs": [
       {
         "name": "_hash",
         "type": "uint256"
       }
     ],
     "name": "viewEncryptedAmount",
     "outputs": [
       {
         "name": "",
         "type": "string"
       }
     ],
     "payable": false,
     "stateMutability": "view",
     "type": "function"
   },
   {
     "constant": true,
     "inputs": [
       {
         "name": "_hash",
         "type": "uint256"
       }
     ],
     "name": "viewSignedData",
     "outputs": [
       {
         "name": "",
         "type": "string"
       }
     ],
     "payable": false,
     "stateMutability": "view",
     "type": "function"
   }
 ]

     var contractAddress_Acme = "0x345ca3e014aaf5dca488057592ee47305d9b3e10";
     var Sc = new web3.eth.Contract(abi_Acme,contractAddress_Acme);

//------------------------------------------------------------------------------
// Llamadas de los usuarios, todas las llamadas deberan hacerse desde funciones 'async'

//-------------------------BANK-------------------------------------------------
  let  pk_b= await  Sc.methods.containsPublicKeyBank( _hash).call(); // retorna el pk_b en string
    //_hash= H(NIF, nºdefactura)   //uint
  // banco encripta con k , firma  y envia a Assignee y Acme  OFF blockchain:
  //  signedData = ((NIF,  nºdefactura,   amount_u,   an_u',  comi.Ac,   anAc  )encryp(k))sign_Bank     // String


  //-------------------------ACME-----------------------------------------------
  await Sc.methods.setPublicKeyBank(_hash,pk_b).send();  // no retorna nada
  //  _hash= H(NIF, nºdefactura)  , //uint

  await  Sc.methods.setSignedAmount( _hash,  signedData,  encryptedAmount).send(); // no retorna nada
  //_hash= H(NIF, nºdefactura) // uint
  //  signedData = recibe de USER
  //  encryptedAmount= (an_b, amount) encryp(R)   //String


  //-------------------------BANK-------------------------------------------------
  let amount =   await  Sc.methods.viewEncryptedAmount(_hash).call(); // retorna String
 await  Sc.methods.bankPayedUser(_hash).call(); // no retorna nada
 await  Sc.methods.govPayedBank(_hash).call(); // no retorna nada
//_hash= H(NIF, nºdefactura) // uint

  //-------------------------DEBTOR-------------------------------------------------
  let amount =   await  Sc.methods.viewEncryptedAmount(_hash).call(); // retorna String
  //_hash= H(NIF, nºdefactura) // uint

  // FUNCIONES QUE PUEDE LLAMAR CUALQUIERA

    let SignedData =   await  Sc.methods.viewSignedData(_hash).call(); // retorna String
    let State =   await  Sc.methods.readState(_hash).call(); // retorna String
    let boolean =   await  Sc.methods.compareStrings(_hash).call(); // retorna True si son iguales

  //---------------------------------------------------------------------------------
  //-------------SMART_CONTRACT_GENE-------------------------------------------------

  //STEVEN MIRATE ESTO PORFA!!!!!
  var abi_gene =[
    {
      "inputs": [],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "constructor"
    },
    {
      "constant": false,
      "inputs": [
        {
          "name": "_hash",
          "type": "uint256"
        }
      ],
      "name": "acceptInvoice",
      "outputs": [],
      "payable": false,
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "constant": true,
      "inputs": [
        {
          "name": "_hash",
          "type": "uint256"
        }
      ],
      "name": "containsInvoice",
      "outputs": [
        {
          "name": "",
          "type": "bool"
        }
      ],
      "payable": false,
      "stateMutability": "view",
      "type": "function"
    }
  ]
     var contractAddress_Gene = "0x345ca3e014aaf5dca488057592ee47305d9b3e10";
     var Sc = new web3.eth.Contract(abi_gene, contractAddress_Gene);

  //--------------------------BANK---------------------------------------------------
     let  boolean= await  Sc.methods.containsInvoice( _hash).call(); // retorna true si encuentra el hash de la factura en la lista de facturas aceptadas de la gene

  //--------------------------DEBTOR-------------------------------------------------
     await  Sc.methods.acceptInvoice(_hash).send(); // no retorna nada
