
// This file will contain and export the functions to connect this project with the smart contract

// Functions have to be exported in order to import them and call them in the components needed

import {debtorSmartContract} from '../smartcontract/smartContractDebtor'
import {smartContracAcme} from '../smartcontract/smartContractAcme'
import Web3 from 'web3';
const web3 = new Web3("http://localhost:8545");

class debtorSCClient {
    constructor () {} 

    async getAccounts(){
        return await web3.eth.getAccounts();
    }
    async containsInvoice(idAndNifHash) {
        return debtorSmartContract.methods.containsInvoice(idAndNifHash).call()
    }

    acceptInvoice(idAndNifHash) {
        const accounts = this.getAccounts().then(accounts => debtorSmartContract.methods.acceptInvoice(idAndNifHash).send({from: accounts[0]}))
    }
}

export const debtorClient = new debtorSCClient()


class acmeSCCLient {
    constructor () {}

    async getAccounts(){
        return await web3.eth.getAccounts();
    }

    // returns true if the public key is stored in the smart contract
    async containsPublicKeyBank(idAndNifHash) {
        return await smartContracAcme.methods.containsPublicKeyBank(idAndNifHash).call()
    }

    setPublicKeyBank(idAndNifHash, publicKey) {
        this.getAccounts().then(accounts => smartContracAcme.methods.setPublicKeyBank(idAndNifHash, publicKey).send({from: accounts[0]}))
    }

    setSignedAmount(idAndNifHash, signedData, encryptedAmount) {
        this.getAccounts().then(accounts => smartContracAcme.methods.setSignedAmount(idAndNifHash, signedData, encryptedAmount).send({from: accounts[0]}))
    }

    async viewEncryptedAmount(idAndNifHash){
        return await smartContracAcme.methods.viewEncryptedAmount(idAndNifHash).call()
    }

    // I guess this method is called to set the invoice process state to last step
    bankPayedUser(idAndNifHash) {
        smartContracAcme.methods.bankPayedUser(idAndNifHash).call()
    }

    // I guess this method is called to set the invoice process state to last step
    govPayedBank(idAndNifHash) {
        smartContracAcme.methods.govPayedBank(idAndNifHash).call()
    }
    
    // returns the signed data stored in the smart contract 
    async viewSignedData(idAndNifHash) {
        return smartContracAcme.methods.viewSignedData(idAndNifHash).call()
    }
    
    // reads the state of the current invoice cession process
    async readState(idAndNifHash) {
        return smartContracAcme.methods.readState(idAndNifHash).call()
    }

    // wtf
    async compareStrings(idAndNifHash) {
        return smartContracAcme.methods.readState(idAndNifHash).call()
    }
}

export const acmeClient = new acmeSCCLient()


