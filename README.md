**Cessions App**
This app simplifies cessions process using React. 
All rights reserved to ACME

To execute the project run the following commands:
    cd pae-frontend
    npm install
    npm start

At database folder you will find a copy of the database json which we used for development. You can import it to your project.

That's all folks, good luck! 